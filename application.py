from flask import Flask, redirect, render_template, make_response, jsonify, request, session, flash
from controllers import users, tokens, songs, downloads, crypter, db_scheduler
from flask_mail import Mail, Message
from blueprints.songsBP import SongBP
from blueprints.userBP import UserBP
from blueprints.downloadsBP import DownBP
import dotenv
import datetime
from flask.json import JSONEncoder
from apscheduler.schedulers.background import BackgroundScheduler

class CustomJSONEncoder(JSONEncoder):
    def default(self, obj):
        if isinstance(obj, users.User) or isinstance(obj, tokens.Token) or isinstance(obj, songs.Song):
            return obj.config()
        else:
            JSONEncoder.default(self, obj)
        
app = Flask(__name__)
app.json_encoder = CustomJSONEncoder

config = dotenv.dotenv_values('.env')
app.config['MAIL_SERVER'] = 'smtp.gmail.com'
app.config['MAIL_PORT'] = 465
app.config['MAIL_USE_TLS'] = False
app.config['MAIL_USE_SSL'] = True
app.config['MAIL_USERNAME'] = 'sound.seller.sas@gmail.com'
app.config['MAIL_PASSWORD'] = 'qHugo12Mpo!'
app.config['SECRET_KEY'] = config['APP_SK']

app.register_blueprint(UserBP)
app.register_blueprint(SongBP)
app.register_blueprint(DownBP)
app.config['PERMANENT_SESSION_LIFETIME'] = datetime.timedelta(hours=2)
mail = Mail(app)
D = downloads.Downloads()
PubA = users.PublicArtist()
Sensor = db_scheduler.Sensor()

def warn_email(user):
    with app.app_context():
        template = open('templates/email/delete_account.html', 'r')
        msg = Message('Compte supprimé',
                    sender=("Apollo", 'sound.seller.sas@gmail.com'),
                    html=template.read().replace('[name]',user['U_name']),
                    recipients=[user['U_email']])
        mail.send(msg)
        return True

def sensor():
    print('SENSING')
    emails = Sensor.action()
    if not emails:
        return
    print('[DB CLEANING] :: Starting')
    for x in emails:
        warn_email(x)
        print(f"   • {x['U_name']} WARNED")
    print('[DB CLEANING] :: Ended')

sched = BackgroundScheduler(daemon=True)
sched.add_job(sensor,'interval',minutes=2)
sched.start()

version = str
with open('config/version_app.txt','r') as v:
    version = v.read()

def send_token():
    token = tokens.Token.get_token(session['user']['id'])
    print(token)
    msg = Message('Création de compte Apollo',
                  sender=("Apollo", 'marty.cremers@gmail.com'),
                  html=render_template(
                      'email/email.html', user=session['user']['name'], token=token.token),
                  recipients=[session['user']['email']])
    mail.send(msg)
    session.clear()
    return True


@app.errorhandler(404)
def page_not_found(e):
    return render_template('http_erro.html', error=e), 404


@app.context_processor
def inject_user():
    u = None
    if 'user' in session:
        u = session['user']
    return dict(not_c=('user' not in session), user=u, version=version)


@app.route('/account')
def account():
    if 'user' in session:
        session['user']['publis'] = PubA.len_published(session['user']['id'])
        f = songs.SongsFeed(session['user']['id']).artist_feed(session['user']['id'])
        return render_template('account.html', feed=f, artist=PubA, downloads=D.len_downloads(session['user']['id'])['count'])
    else:
        return redirect('/')

@app.route('/')
def index():
    if 'user' in session:
        if 'id' in session['user']:
            page = request.args.get('page')
            f = songs.SongsFeed(session['user']['id'])
            if not page:
                return render_template('feed.html', feed=f.feed(), artist=PubA)
            else:
                feed = f.feed(page=int(page))
                return render_template('feed.html', feed=feed, artist=PubA)

    return redirect('/welcome')


@app.route('/welcome')
def welcom():
    if 'user' in session:
        return redirect('/')
    return render_template('index.html')


@app.route('/new-post')
def new_post():
    if 'user' in session:
        if session['user']['type'] == "A":
            if session['user']['ready']:
                return render_template('new_post.html')
            else:
                flash('Vous devez valider votre compte de payement.')
                return redirect('/u/sf')
        else:
            return render_template('http_erro.html')
    else:
        return redirect('/')


@app.route('/explore')
def explore():
    if not 'user' in session:
        flash('Les recherches requièrent votre connexion', 'bad')
        return redirect('/')
    return render_template('explore.html')


@app.route('/register')
def register():
    if 'user' in session:
        return redirect('/')
    image = crypter.Verif_image().config()['hash']
    session['verif_image'] = image
    return render_template('register.html', image_src=image)


@app.route('/logout')
def logout():
    session.clear()
    return redirect('/')


@app.route('/artist/<artist_name>')
def artist_profile(artist_name):
    if artist_name == session['user']['name']:
        return redirect('/account')
    artist = users.PublicArtist(artist_name)
    u = users.User(session['user']['id'])
    return render_template('artist.html', artist=artist.info[0], following=u.is_following(artist.info[0]['U_id']))


@app.route('/login')
def login():
    if "user" in session:
        return redirect('/')
    return render_template('login.html')


@app.route('/new', methods=['POST'])
def new_sound():
    req = request.json
    image = crypter.Verif_image(session['verif_image'])
    if not image.decrypt(req['verif_image']):
        return make_response(jsonify(status='bot'))
    user = users.User.new(
        {'email': req['email'], 'name': req['name'], 'password': 'coucou'}
    )
    if user == 'exists':
        return make_response(jsonify(status='exists', url='/u/s'))
    session['user'] = user.config()
    session.modified = True
    send_token()
    return make_response(jsonify(status='ok', url='/u/s'))

@app.route('/return')
def return_stripe():
    return render_template('stripe_templates/return.html')


@app.route('/reauth')
def reauth_stripe():
    return render_template('stripe_templates/reauth.html')

@app.route('/go-premium')
def premium_subscribe():
    return render_template('stripe_templates/premium.html')

@app.route('/support', methods=['GET','POST'])
def support():
    if request.method == 'GET':
        return render_template('support.html')
    elif request.method == 'POST':
        msg = Message('Message Apollo',
                  sender=("Apollo", 'marty.cremers@gmail.com'),
                  html=f""" «{request.form['topic']}»\nNouveau message de <code>{request.form['email']}</code> :\n<code>{request.form['content']}</code> """,
                  recipients=["martinsites@protonmail.com"])
        mail.send(msg)
        flash('Message envoyé','good')
        return redirect('/support')
    else:
        return render_template('http_erro.html')


@app.route('/test')
def test():
    return render_template('test.html')

if __name__ == "__main__":
    app.run(debug=True)
    sched.shutdown()
