version=[0,0,0]
with open('config/version_app.txt','r') as v:
    version = v.read().split('.')

if version == ['']:
    version = [0,0,0]
for x in range(len(version)):
    version[x]= int(version[x])
version[2] = version[2] + 1
for x in reversed(range(len(version))):
    if version[x] == 10 and x>0:
        version[x-1], version[x] = version[x-1]+1, 0

with open('config/version_app.txt', 'w') as v:
    str_version = ''
    for x in version:
        str_version+=str(x) + '.'
    str_version = str_version[:-1]
    v.write(str_version)

print(f'Updated from deplyment New version: {version[0]}.{version[1]}.{version[2]}')