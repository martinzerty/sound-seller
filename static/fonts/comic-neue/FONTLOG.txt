
  FONTLOG: Comic Neue

  --------------------------------------------
  Comic Neue Light
  --------------------------------------------

  This file provides detailed information on the Comic Neue 
  Font Software.
  This information should be distributed along with the Comic 
  Neue fonts and any derivative works.
  
  Comic Neue Font Information
  
  Comic Neue is a Unicode typeface family that supports the 
  languages
  outlined in the following two ISO standards:
  Latin 2 http://en.wikipedia.org/wiki/ISO/IEC_8859-15
  Latin 9 http://en.wikipedia.org/wiki/ISO/IEC_8859-15
  
  Documentation can be found at www.comicneue.com
  
  ChangeLog
  
  23 May 2015 (Christoph Haag) 
- created fontforge sources as base to work on GNU/Linux
- added FONTLOG to .sfdir
- Mastered Font from Fontforge SFDIR to 
  UFO,OTF,TTF,EOT,WOFF,SVG
  
  25 December 2014 (Craig Rozynski) Comic Neue version 2.1
- Added SIL Open Font License.
  
  22 September 2014 (Hrant Papazian) Comic Neue version 2.0
  – Refinements to glyphs and metrics.
  
  7 April 2014 (Craig Rozynski) Comic Neue version 1.0
  – Publicly launched intial iteration of the font.
  
  Acknowledgements
  
  If you make modifications be sure to add your name (N), 
  email (E), web-address
  if you have one) (W) and description (D). This list is in 
  alphabetical order.
  
  N: Christoph Haag
  E: christoph@lafkon.net
  W: www.lafkon.net
  D: Transformer
  
  N: Hrant Papazian
  E: hpapazian@gmail.com
  W: http://themicrofoundry.com
  D: Contributor - Glyph and metric refinement
  
  N: Craig Rozynski
  E: craig@craigrozynski.com
  W: http://www.comicneue.com
  D: Contributor - Original glyphs


  --------------------------------------------
  Comic Neue Angular
  --------------------------------------------
  see above
  --------------------------------------------
  Comic Neue Oblique
  --------------------------------------------
  see above
  --------------------------------------------
  Comic Neue Angular Oblique
  --------------------------------------------
  see above
  --------------------------------------------
  Comic Neue Angular Light Oblique
  --------------------------------------------
  see above
  --------------------------------------------
  Comic Neue
  --------------------------------------------
  see above
  --------------------------------------------
  Comic Neue Angular Bold
  --------------------------------------------
  see above
  --------------------------------------------
  Comic Neue Angular Light
  --------------------------------------------
  see above
  --------------------------------------------
  Comic Neue Light Oblique
  --------------------------------------------
  see above
  --------------------------------------------
  Comic Neue Bold Oblique
  --------------------------------------------
  see above
  --------------------------------------------
  Comic Neue Angular Bold Oblique
  --------------------------------------------
  see above
  --------------------------------------------
  Comic Neue Bold
  --------------------------------------------
  see above
