import os
from flask import Blueprint, flash, render_template, send_file, session,make_response, jsonify, request, redirect
from controllers import users, tokens, file_handler, crypter, songs, payment, downloads

DownBP = Blueprint('download', __name__, url_prefix='/d', template_folder='download_templates')

@DownBP.before_request
def inspect_session():
    if not 'user' in session:
        flash('Soyez connectez')
        return redirect('/')

@DownBP.route('/')
def index():
    return redirect('/d/l')

@DownBP.route('/l')
def list_downloads():
    d = downloads.Downloads().get_downloads(session['user']['id'])
    return render_template('download_templates/list.html', downloads=d, long=len(d))

@DownBP.route('/save/<id>')
def save_download(id):
    songId= downloads.Downloads().single_download(id, session['user']['id'])['D_downloadedItem']
    if not songId:
        flash('Fichier inéxistant')
        return redirect('/d/l')
    song = songs.Song(songId)
    url = song.path
    if downloads.Downloads().can_download(id, session['user']['id']):
        try:
            download_name = file_handler.save_song(url)
            res = send_file(download_name, as_attachment=True)
            os.remove(download_name)
            downloads.Downloads().increment(id)
            return res
        except Exception as e:
            print(e)
    flash('Fichier inéxistant')
    return redirect('/d/l')


@DownBP.route('/e', methods=['POST', 'GET'])
def edit():
    if request.method == 'GET':
        return redirect('/account')

    form = dict(request.form)
    default_values = {
        'e_visible': False,
        'd_visible': False,
        'website': '',
        'visible_email': ''
    }

    bool_values = ['e_visible', 'd_visible']

    for x in ['e_visible', 'd_visible', 'website', 'visible_email']:
        if x not in form:
            form[x] = default_values[x]
        else:
            if x in bool_values:
                form[x] = form[x] == 'on'
    try:
        user = users.User(session['user']['id'])
        user.edit(
            username=form['name'],
            email=form['email'],
            e_visible=form['e_visible'],
            d_visible=form['d_visible'],
            website=form['website'],
            public_email=form['visible_email']
        )
        session['user'] = user.config()
    except Exception as e:
        print(e)
        flash('Vérifiez les informations')
        return redirect(request.url)
    
    flash('Modifié')
    return redirect(request.url)

@DownBP.route('/<id>')
def single_download(id):
    d = downloads.Downloads().single_download(id, session['user']['id'])
    return render_template('download_templates/single.html', download = d)