from time import strftime
from flask import Blueprint, flash, render_template, session,make_response, jsonify, request, redirect, current_app
from controllers import users, tokens, file_handler, crypter, songs, songs, payment
from controllers.downloads import Downloads
from pydub import AudioSegment
from pydub.utils import make_chunks
from flask_mail import Mail, Message
import os

SongBP = Blueprint('song', __name__, url_prefix='/s')
PubA = users.PublicArtist()

@SongBP.route('/p', methods=['POST'])
def valid():
    file      = request.files['file']
    duration  = request.form['duration']
    title     = request.form['titre']
    price     = request.form['price']
    tags_list = request.form['tags'].split(',')

    filename = crypter.gen_hash()+'.'+file.filename.split('.')[-1]
    chunk_name = "CHUNK_"+filename

    file.save(f'aws/chunks/{chunk_name}')
    song = AudioSegment.from_file(f'aws/chunks/{chunk_name}', chunk_name.split('.')[-1])
    chunk = make_chunks(song, 10000)[0]
    
    if not file_handler.upload_sond(f'aws/chunks/{chunk_name}', filename):
        return make_response(jsonify(result='Erreur de la mise en ligne du fichier audio'))

    os.remove(f'aws/chunks/{chunk_name}')
    chunk.export(f'aws/chunks/{chunk_name}', format=chunk_name.split('.')[-1])
    if not file_handler.upload_sound_chunk( chunk_name):
        return make_response(jsonify(result='Erreur de la mise en ligne de l\'extrait audio'))
    os.remove(f'aws/chunks/{chunk_name}')
    path=f"https://muse-fabric.s3.eu-west-3.amazonaws.com/{filename}"
    if not songs.Song.new({'title':title, 'price':int(price), 'path':path, 'length':duration}, tags=tags_list, userId=session['user']['id']):
        return make_response(jsonify(result='Erreur lors de la mise en ligne des données'))
    return make_response(jsonify(result='ok'))

@SongBP.route('/a/<artist_id>')
def artist_feed(artist_id):
    return make_response(jsonify(status='ok', songs=songs.SongsFeed(session['user']['id']).artist_feed(artist_id)))

@SongBP.route('/e/<search>')
def explore(search:str):
    if 'user' not in session:
        return make_response(jsonify(status='not connected'))
    feed = songs.SongsFeed(session['user']['id']).from_tag(search)
    return make_response(jsonify(status='ok', result=feed))


@SongBP.route('/pay', methods=['POST'])
def pay_song():
    song_id = request.form['song_id']
    song = songs.Song(song_id).attrs()
    product_id = song['product_id']
    if Downloads().already_owned(session['user']['id'], song_id):
        flash('Vous possédez déjà cette oeuvre, details dans votre acquisitions.', 'good')
        return redirect('/')
    return render_template('stripe_templates/payment.html', price_id=product_id, song_id=song['id'], song=song['title'], price=song['price'])

@SongBP.route('/c-p-i',methods=['POST'])
def create_payment_intent():
    account_id = songs.Song(request.json['item']['song_id']).author_account()['U_acc_id']
    intent = payment.Product().create_payment(request.json['item']['id'], account_id)
    d = Downloads().add_one({
        "downloaderId": session['user']['id'],
        "downloadedItem": request.json['item']['song_id'],
        "intent": intent['id']  
    })
    return jsonify({
            'clientSecret': intent['client_secret']
        })

@SongBP.route('/pay-success')
def payment_success():
    payment_intent = request.args.get('payment_intent')
    intent = payment.Product().retrieve(payment_intent)
    song = songs.Song.from_intent(intent['id'])
    d = Downloads().finish(intent['id'])
    a = users.PublicArtist().get_email([song.artist_id])
    song.add_buy()
    msg = Message('Création de compte Apollo',
                  sender=("Apollo", 'marty.cremers@gmail.com'),
                  html=render_template(
                      'email/payment_email.html',name=song.title, prix=song.price, final_price=(song.price-1.25), heure=strftime('%H:%M:%S')),
                  recipients=[a])
    with current_app.app_context():
        mail = Mail() 
        mail.send(msg)

    return render_template('stripe_templates/return_payement.html', intent=intent)

@SongBP.route('/i', methods=['POST'])
def song_infos():
    song = songs.Song(int(request.json['song_id'])).attrs()
    if 'payment' in request.json:
        intent = payment.Product().retrieve(request.json['payment'])
        intent = dict({
            'amount': intent['amount'],
            'payment_method_types': intent['payment_method_types'],
            'receipt': intent['charges']['data'][0]['receipt_url']
        })
        song.update(intent)
    return make_response(jsonify(song), 200)

@SongBP.route('/render', methods=['POST'])
def render_song():
    body = request.json
    return render_template('partials/song.html', x=body['song'], artist=PubA, in_artist_page=True)