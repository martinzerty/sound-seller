import re
from flask import Blueprint, flash, render_template, session,make_response, jsonify, request, redirect, current_app
from flask_mail import Mail, Message
from controllers import users, tokens, file_handler, crypter, payment
from controllers.payment import client
from controllers.send_email import email_sender

UserBP = Blueprint('user', __name__, url_prefix='/u')

def check_username(username):
    return users.User.is_taken(username)

def email_verif(email):
    regex = r'\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Z|a-z]{2,}\b'
    return re.fullmatch(regex, email)

def resend_token(token):
    token = tokens.Token.reset(token)
    msg = Message('Création de compte Apollo',
                  sender=("Apollo", 'marty.cremers@gmail.com'),
                  html=render_template(
                      'email/email.html', user=session['user']['name'], token=token.token),
                  recipients=[session['user']['email']])
    
    with current_app.app_context():
        mail = Mail() 
        mail.send(msg)
    return True

def send_reset_token(token, name, email):
    tem = render_template(
                      'email/reset_password_email.html', name=name, token=token.token)
    return email_sender.send('reset', tem, [email])

@UserBP.route('/infos/<id>')
def pulic_profile(id):
    artist = users.PublicArtist().artist_infos([int(id)])
    return make_response(jsonify(artist=artist))

@UserBP.route('/taken/<username>')
def is_taken(username):
    return make_response(jsonify(taken=check_username(username)), 200)

@UserBP.route('/v/<token>')
def valid(token):
    result, user = tokens.Token.verify_token(token)
    if not result:
        return render_template('error.html', err=user, error_type_file='TOK')
    try:
        ready = user.ready
    except Exception as e:
        print(e)
        flash('Aucun compte n\'est lié à ce token.')
        return redirect('/logout')
    if ready:
        flash('Votre compte est déjà finalisé !')
        return redirect('/login')
    else:
        return render_template('complete_register.html', user=user.config())

@UserBP.route('/v', methods=['POST'])
def finalize_usr():
    js = request.form
    file = request.files['file']
    filename=crypter.gen_hash() + '.' + file.filename.split('.')[-1]
    file_handler.upload_file_to_s3(file, filename=filename)

    UserId = tokens.Token.retrieve_elem(js['token'])

    u = users.User(UserId)
    result =  u.finalize(js['password'], js['type'], filename,public_email= js['public_email'], website=js['website'], visible={'e': js['e_visible'], 'd': js['d_visible']}, )
    if not result:
        flash('Une erreur est arrivée', 'bad')
        return redirect(request.url)
    session.clear()
    return make_response(jsonify(success=True), 200)

@UserBP.route('/resend', methods=['POST'])
def resend_token():
    token = request.form['token']
    resend_token(token)
    return redirect('/u/s')

@UserBP.route('/l', methods=['POST'])
def login():
    form = request.form
    if 'user' in session:
        if 'id' in session['user']:
            return redirect('/')
    res, reason = users.User.login(form['email'], form['password'])
    if res:
        session['user'] = reason.config()
        print('HAS TO CONFIG ?:', (not reason.ready) and (reason.type.upper == 'A'), " READY ?:", not reason.ready, "ARTIST ? :", reason.type == 'A')
        if (not reason.ready) and (reason.type.upper() == 'A'):
            return redirect('/u/sf')
        return redirect('/')
    else:
        flash(f'Mauvais {reason}', 'bad')
        return redirect('/login')

@UserBP.route('/f/<id>')
def follow_switch(id):
    user = users.User(session['user']['id'])
    user.switch_follow(id)
    session['user']=user.config()
    return make_response(jsonify(status='ok'))

@UserBP.route('/sf', methods=['GET','POST'])
def stripe_finish():
    if not 'user' in session:
        flash('Connectez vous pour avoir accès aux paramètres')
        return redirect('/')
    if request.method == 'GET':
        if session['user']['type'].upper() != 'A':
            flash('On vous en demandera pas plus, promis !')
            return redirect('/')
        return render_template('stripe_templates/connected_account.html')
    else:
        u = users.User(session['user']['id'])
        u.finalize_stripe(request.form['token-account'], session['user']['website'], request.form['iban'], session['user']['email'])
        session['user']['ready']=True
    flash('Compte stripe finalisé')
    return redirect('/')

@UserBP.route('/s')
def success():
    return render_template('success_templates/success_email.html')

@UserBP.route('/sc')
def success_creation():
    return render_template('success_templates/s_user_creation.html')

@UserBP.route('/c')
def cancel():
    u = users.User(session['user']['id'])
    res = u.cancel()
    if res:
        session['user']=None
    return make_response(jsonify(canceled=res))

@UserBP.route('/reset', methods=['POST'])
def reset_pwd():
    infos={}
    if 'email_reset' in request.form:
        if email_verif(request.form['email_reset']):
            try:
                userId = users.User.get_id(request.form['email_reset'])
                username = users.User.get_username(userId)

                infos['elem_id'] = userId
                tok_content = crypter.gen_hash()
                infos['token'] = tok_content

                print(infos)
                token = tokens.Token.new(
                    infos=infos
                )
                
                send_reset_token(
                    token,
                    username,
                    request.form['email_reset']
                )
                flash('Checkez votre boite mail, ça doit y être.')
            except Exception as e:
                flash('Non disponible avec cet e-mail.')
                print('ERROR password reset - get id : ',e)
        else:
            flash('Non disponible avec cet e-mail.')
            return render_template('reset_password.html', ask="email")
    return redirect("/login")
    

@UserBP.route('/g-p', methods=['POST'])
def go_premium():
    intent = payment.Product().create_payment(request.json['item']['id'], "acct_1Khst6LPL3RO6EtM")
    u = users.User(session['user']['id'])
    u.go_premium(session['user']['id'])
    return jsonify({
            'clientSecret': intent['client_secret']
        })