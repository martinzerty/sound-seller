import dotenv
import psycopg2
from psycopg2.extras import RealDictCursor
from controllers import payment
from controllers.users import User


class SongsFeed:
    def __init__(self, UserId) -> None:
        self.config = dotenv.dotenv_values('.env')
        self.con = psycopg2.connect(self.config['DB_URL'])
        self.cur = self.con.cursor(cursor_factory=RealDictCursor)
        self.user = User(UserId)

    def feed(self,artist=None, page=1):
        offset = (page-1)*7
        self.cur.execute("""
            SELECT * FROM songs
            JOIN followings ON followings."F_artist_id"=songs."S_artist_id"
            JOIN users ON followings."F_follower_id"=users."U_id"
            WHERE "U_id"=%s
            AND NOT EXISTS (
                SELECT * FROM downloads WHERE "D_downloadedItem"="S_id" AND "D_downloaderId"="U_id"
            )
            ORDER BY (
                (EXTRACT(EPOCH FROM (CURRENT_TIMESTAMP - "S_created_at") ::INTERVAL) / 60) * 10 +
                ("U_publis") * 25 +
                ("S_buy_number") * 24 +
                ("U_followers" / "U_followings") * 41
            ) / 100
            DESC LIMIT 7 OFFSET %s;
        """, [self.user.id, offset])
        res = {
            'feed':self.cur.fetchall(),
            'count':self.len_feed()['count'],
            'page':page
        }
        return res
    
    def len_feed(self,artist=None):
        self.cur.execute("""
            SELECT DISTINCT COUNT(*) FROM songs
            JOIN followings ON followings."F_artist_id"=songs."S_artist_id"
            JOIN users ON followings."F_follower_id"=users."U_id"
            WHERE "U_id"=%s
            AND NOT EXISTS (
                SELECT * FROM downloads WHERE "D_downloadedItem"="S_id" AND "D_downloaderId"="U_id"
            );
        """, [self.user.id])
        count = self.cur.fetchone()
        return count


    def artist_feed(self, artist_id):
        self.cur.execute(
            """
            SELECT * FROM songs WHERE "S_artist_id"=%s ORDER BY "S_created_at" DESC;
            """, [int(artist_id)]
        )
        return self.cur.fetchall()
    
    def from_tag(self, tag):
        self.cur.execute(
           f"""SELECT DISTINCT "S_id", "S_artist_id", "S_title", "S_created_at",  "S_length", "S_price" FROM songs 
           JOIN tag_item ON tag_item."Item_id"=songs."S_id"
           JOIN tag ON tag."Tag_id"=tag_item."Tagi_id"
           WHERE UPPER("Tag_title") LIKE UPPER('%{tag.upper()}%') OR LOWER("S_title") LIKE LOWER('%{tag}%')
           ORDER BY "S_created_at" DESC LIMIT 15;
            """
        )
        rows = self.cur.fetchall()
        songs = [dict(row) for row in rows]
        for x in songs:
            x['objectType'] = 'SONG'
        self.cur.execute(f"""
        SELECT DISTINCT "U_id", "U_name", "U_thumbURL", "U_followers", "U_publis", "U_created_at"  FROM users WHERE LOWER("U_name") LIKE LOWER('%{tag}%') ORDER BY "U_created_at" DESC LIMIT 5
        """)
        rows = self.cur.fetchall()
        users = [dict(row) for row in rows]
        for x in users:
            x['objectType'] = 'USER'
        objects = songs + users
        return objects

class Song:

    @staticmethod
    def from_intent(intent):
        config = dotenv.dotenv_values('.env')
        con = psycopg2.connect(config['DB_URL'])
        cur = con.cursor(cursor_factory=RealDictCursor)
        cur.execute('SELECT "S_id" FROM songs JOIN downloads ON downloads."D_downloadedItem"=songs."S_id" WHERE "D_intent"=%s',[intent])
        song_id = cur.fetchone()['S_id']
        con.close()
        return Song(song_id)

    @staticmethod
    def new(infos, userId,tags = []):
        try:
            config = dotenv.dotenv_values('.env')
            con = psycopg2.connect(config['DB_URL'])
            cur = con.cursor(cursor_factory=RealDictCursor)

            infos['artist_id'] = userId
            infos['product_id'] = payment.Product().create_product(infos['title'], infos['price']).id
            querry = "INSERT INTO songs( "
            for x in infos:
                querry += f""" "S_{x}","""
            querry = querry[:-1] + ') VALUES('
            values = []
            for y in infos:
                values.append(infos[y])
                querry += f"%s,"
            querry = querry[:-1] + ') RETURNING "S_id";'
            cur.execute(querry, values)
            row = cur.fetchone()
            con.commit()
            if len(tags) > 0:
                tags = [x.upper() for x in tags]

                """-------Add tag to tag title if it is not existing"""
                query = """INSERT INTO tag ("Tag_title") VALUES"""
                for x in tags:
                    query+= "(%s),"
                query=query[:-1] + """ ON CONFLICT ON CONSTRAINT "tag_Tag_title_key" DO NOTHING"""
                cur.execute(query, tags)
                con.commit()

                query = """SELECT "Tag_id" FROM tag WHERE """
                for tag in tags:
                    query+=""" "Tag_title"=%s OR"""
                query = query[:-2]
                cur.execute(query, tags)
                tag_ids = cur.fetchall()
                tag_ids = [ x['Tag_id'] for x in tag_ids]
                """--------link tags to song by tag_item table"""
                query = """INSERT INTO tag_item ("Item_id","Tagi_id") VALUES"""
                for x in tag_ids:
                    query+= f"({row['S_id']},%s),"
                query=query[:-1]
                cur.execute(query, tag_ids)
                con.commit()
            
            user=User(userId)
            user.add_publis()

            return Song(row['S_id'])
        except Exception as e:
            print(e)
            return False

    def __init__(self, id) -> None:
        self.id = id
        self.config = dotenv.dotenv_values('.env')
        self.con = psycopg2.connect(self.config['DB_URL'])
        self.cur = self.con.cursor(cursor_factory=RealDictCursor)
        self.fill_sound_infos()


    def fill_sound_infos(self):
        try:
            self.cur.execute(f"SELECT * FROM songs WHERE \"S_id\"={self.id}")
            infos = self.cur.fetchone()
            for x in infos:
                setattr(self,x.split('_', 1)[1], infos[x])
        except Exception as e:
            print(e)
        
    def author_account(self):
        self.cur.execute('SELECT "U_acc_id" FROM users JOIN songs ON users."U_id"=songs."S_artist_id"  WHERE "S_id"=%s', [self.id])
        return self.cur.fetchone()
    
    def add_buy(self):
        self.cur.execute('UPDATE songs SET "S_buy_number"= "S_buy_number"+1 WHERE "S_id"=%s', [self.id])
        self.con.commit()
        return True
    
    def attrs(self):
        return {
            'id':self.id,
            'artist_id':self.artist_id,
            'title':self.title,
            'length':self.length,
            'buy_number':self.buy_number,
            'created_at': str(self.created_at),
            'price': self.price,
            'product_id': self.product_id,
        }
