DO $$
BEGIN
IF EXISTS (SELECT "F_id" FROM followings WHERE "F_artist_id"=%s AND "F_follower_id"=%s) THEN
    DELETE FROM followings WHERE "F_artist_id"=%s AND "F_follower_id"=%s;
    UPDATE users SET "U_followers"="U_followers" - 1 WHERE "U_id"=%s;
    UPDATE users SET "U_followings"="U_followings" - 1 WHERE "U_id"=%s;
ELSE
INSERT INTO followings
    ("F_artist_id","F_follower_id") VALUES(%s, %s);
    UPDATE users SET "U_followers"="U_followers" + 1 WHERE "U_id"=%s;
    UPDATE users SET "U_followings"="U_followings" + 1 WHERE "U_id"=%s;
END IF;
END
$$