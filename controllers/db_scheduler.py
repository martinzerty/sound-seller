import dotenv
import psycopg2
from psycopg2.extras import RealDictCursor
from controllers import payment
from controllers.users import User


class Sensor:
    def __init__(self) -> None:
        self.__config = dotenv.dotenv_values('.env')
        self.__con = psycopg2.connect(self.__config['DB_URL'])
        self.__cur = self.__con.cursor(cursor_factory=RealDictCursor)
        self.USER_INTEVAL = 2880 # in minutes

    def action(self):
        if not self.remove_tokens():
            return False
        if not self.remove_downloads():
            return False
        email = self.remove_users()
        return email

    def remove_tokens(self):
        try:
            self.__cur.execute('DELETE FROM tokens WHERE "TOK_end"<CURRENT_TIMESTAMP;')
            self.__con.commit()
        except Exception as e:
            print('REMOVE [TOKEN] ERROR => ', e)
            return False
        return True
    
    def remove_users(self):
        try:
            self.__cur.execute("""DELETE FROM users 
            WHERE ((NOT "U_ready") AND
            "U_type" != %s AND
            ((EXTRACT(EPOCH FROM (CURRENT_TIMESTAMP - "U_created_at") ::INTERVAL) / 60) > %s))
            RETURNING "U_email", "U_name" """, ['A',self.USER_INTEVAL])
            emails = self.__cur.fetchall()
            self.__con.commit()
            return emails
        except Exception as e:
            print('REMOVE [USERS] ERROR => ', e)
            return False
        
    def remove_downloads(self):
        try:
            self.__cur.execute("""
                DELETE FROM downloads WHERE NOT "D_buyed"
            """)
            self.__con.commit()
            return True
        except Exception as e:
            print('REMOVE [DOWNLOADS] ERROR => ',e)