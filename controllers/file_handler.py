import secrets
import boto3
from controllers.configaws import S3_KEY, S3_SECRET, S3_BUCKET
from controllers import crypter
from PIL import Image
import io
import string

def resize(file):
    img = Image.open(file)

    # When image height is greater than its width
    if img.height > img.width:
        # make square by cutting off equal amounts top and bottom
        left = 0
        right = img.width
        top = (img.height - img.width)/2
        bottom = (img.height + img.width)/2
        img = img.crop((left, top, right, bottom))
        # Resize the image to 300x300 resolution
        if img.height > 300 or img.width > 300:
            output_size = (300, 300)
            img.thumbnail(output_size)

    # When image width is greater than its height
    elif img.width > img.height:
        # make square by cutting off equal amounts left and right
        left = (img.width - img.height)/2
        right = (img.width + img.height)/2
        top = 0
        bottom = img.height
        img = img.crop((left, top, right, bottom))
        # Resize the image to 300x300 resolution
        if img.height > 300 or img.width > 300:
            output_size = (300, 300)
            img.thumbnail(output_size)
    return img


s3 = boto3.client(
    "s3",
   aws_access_key_id=S3_KEY,
   aws_secret_access_key=S3_SECRET
)

def upload_sond(file, filename):
    if filename =='':
        filename = file.filename

    try:
        with open(file, 'rb') as f:
            result = s3.upload_fileobj(
                f,
                S3_BUCKET,
                'songs/'+filename
            )
            return "don"
    except Exception as e:
        print(e)
        return False

def upload_sound_chunk(filename):


    try:
        with open(f"aws/chunks/{filename}", "rb") as f:
            result = s3.upload_fileobj(
                f,
                S3_BUCKET,
                'chunks/'+filename
            )
            return "don"
    except Exception as e:
        return False

def upload_file_to_s3(file, filename='', acl="public-read"):

    """
    Docs: http://boto3.readthedocs.io/en/latest/guide/s3.html
    """
    if filename =='':
        filename = file.filename

    img = Image.open(file)
    img.thumbnail((300,300))

    thumb = io.BytesIO()
    img.save(thumb, format=img.format)
    thumb.seek(0)
    try:
        result = s3.upload_fileobj(
            thumb,
            S3_BUCKET,
            'thumbs/'+filename
        )


        return "don"
    except Exception as e:
        print(e)
        return False
    

def save_song(path):
    key = 'songs/'+path.rsplit('/', 1)[-1]
    download_name = crypter.gen_hash() + '.' + key.split('.')[-1]
    s3.download_file('muse-fabric', key, Filename=download_name)
    return download_name
    

def delete_file_from_s3(file):
    try:
        s3.delete_object(
            Bucket=S3_BUCKET,
            Key=file
        )
        return True
    except Exception as e:
        print(e)
        return False


def erase():
    response = s3.list_objects_v2(Bucket=S3_BUCKET)
    files = response.get("Contents")
    for file in files:
        print(f"file_name: {file['Key']}, size: {file['Size']}")
