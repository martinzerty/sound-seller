from mailjet_rest import Client
import os
import json

api_key = ''
api_secret = ''
mailjet = Client(auth=(api_key, api_secret), version='v3.1')

def send(template_id, template, recipients):
  with open('controllers/send_email/subjects.json', 'r') as fb:
    subject = json.load(fb)
  print(subject)
  data = {
    'Messages': [
      {
        "From": {
          "Email": "sound.seller.sas@gmail.com",
          "Name": "Apollo"
        },
        "To": [{"Email":x} for x in recipients],
        "Subject": subject[template_id],
        "TextPart": "",
        "HTMLPart": template
      }
    ]
  }
  result = mailjet.send.create(data=data)
  return result.json()['Messages'][0]['Status'] == 'Success'