import psycopg2
from psycopg2.extras import RealDictCursor
import dotenv

class Downloads:
    def __init__(self):
        self.__config = dotenv.dotenv_values('.env')
        self.__con = psycopg2.connect(self.__config['DB_URL'])
        self.__cur = self.__con.cursor(cursor_factory=RealDictCursor)

    def len_downloads(self, userId):
        self.__cur.execute("""SELECT COUNT(*) FROM downloads WHERE "D_downloaderId"=%s AND "D_buyed" """, [userId])
        return self.__cur.fetchone()

    def add_one(self, infos={}):
        """
         --infos: required informations {
             'downloaderId': str,
             'downloadedItem': int,
             'intent': str
         }
        """
        query = "INSERT INTO downloads ("
        for x in infos:
            query+= f' "D_{x}",'
        query = query[:-1]+") VALUES ("
        values = []
        for x in infos:
            query += " %s,"
            values.append(infos[x])
        query = query[:-1] +")"
        self.__cur.execute(query, values)
        self.__con.commit()
        return True

    def increment(self, dId):
        self.__cur.execute('UPDATE downloads SET "D_downoaldsNbr"="D_downoaldsNbr"+1 WHERE "D_id"=%s', [dId])
        self.__con.commit()
        return True
    
    def get_downloads(self, userId):
        self.__cur.execute("""SELECT * FROM downloads WHERE "D_downloaderId"=%s AND "D_buyed" """, [userId])
        return self.__cur.fetchall()
    
    def single_download(self, d_id, userId):
        self.__cur.execute(""" SELECT * FROM downloads WHERE "D_id"=%s AND "D_downloaderId"=%s """, [d_id, userId])
        return self.__cur.fetchone()
    
    def can_download(self, d_id, userId):
        self.__cur.execute("""SELECT * FROM downloads WHERE "D_id"=%s AND "D_downloaderId"=%s AND "D_downoaldsNbr" < 4""", [d_id, userId])
        row = self.__cur.fetchone()
        return row != None

    def finish(self, d_intent):
        self.__cur.execute('UPDATE downloads SET "D_buyed"=true WHERE "D_intent"=%s',[d_intent])
        self.__con.commit()
        return True
    
    def already_owned(self, U_id, S_id):
        self.__cur.execute('SELECT COUNT(*) FROM downloads WHERE "D_downloaderId"=%s AND "D_downloadedItem"=%s AND "D_buyed"', [U_id, S_id])
        return self.__cur.fetchone()['count'] > 0