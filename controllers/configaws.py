import dotenv

config=dotenv.dotenv_values('.env')

S3_KEY       = config['S3_KEY']
S3_SECRET    = config['S3_SECRET']
S3_BUCKET    = config['S3_BUCKET']
