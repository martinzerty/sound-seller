import dotenv
import stripe


class Client:
    def __init__(self):
        self.config = dotenv.dotenv_values('.env')
        self.sk = self.config['STR_SK']
        self.pk = self.config['STR_PK']

        stripe.api_key = self.sk

    def create_cus(self, email, name):
        cus = stripe.Customer.create(
            email=email,
            description=f'Utilisateur {name}'
        )
        return cus.id

    def create_account(self, token_acc, website, iban):
        account = stripe.Account.create(
            country="FR",
            type="custom",
            capabilities={
                "card_payments": {"requested": True},
                "transfers": {"requested": True},
            },
            account_token=token_acc,
            external_account={
                "object":'bank_account',
                "country":'FR',
                "currency":'eur',
                "account_number": iban
            },
            business_profile={
                "mcc": "5815",
                "url": website
            }
        )

        return account