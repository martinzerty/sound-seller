import dotenv
import stripe


class Product:
    def __init__(self):
        self.config = dotenv.dotenv_values('.env')
        self.sk = self.config['STR_SK']
        self.pk = self.config['STR_PK']

        stripe.api_key = self.sk

    def create_product(self, title, price):

        product = stripe.Price.create(
            currency='eur',
            unit_amount=price*100,
            product_data={
                'name': title
            }
        )
        return product

    def retrievePrice(self, priceId):
        return stripe.Price.retrieve(priceId).unit_amount

    def create_payment(self, price, account_id):
        amount = self.retrievePrice(price)
        """if the accountId is the same than 'my' accountId, there won't be transfer_data"""
        if account_id == 'acct_1Khst6LPL3RO6EtM':
            intent = stripe.PaymentIntent.create(
                amount=amount,
                currency='eur',
            )
        else:
            intent = stripe.PaymentIntent.create(
                amount=amount,
                currency='eur',
                application_fee_amount=123,
                transfer_data={
                    'destination': account_id
                }
            )
        return intent

    def retrieve(self, intent):
        return stripe.PaymentIntent.retrieve(intent)
