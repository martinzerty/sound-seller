from cryptography.fernet import Fernet
import dotenv
import hashlib
import secrets
import os

config=dotenv.dotenv_values('.env')
f = Fernet(config['FERNET_KEY'].encode('utf-8'))

def hash(var: str) -> str:
    h = hashlib.sha256()
    h.update(var.encode('utf-8'))
    return h.hexdigest()

def gen_hash() -> str:
    return secrets.token_hex(16)

def crypt(var: str) -> str:
    return f.encrypt(var.encode('utf-8')).decode('utf-8')

def decrypt(var: str) -> str:
    return f.decrypt(var.encode('utf-8')).decode('utf-8')

class Verif_image:
    def __init__(self, hash=None):
        if hash:
            self.__image = hash+'.png'
        else:
            self.__image = secrets.choice(os.listdir('static/verification_letters'))
        self.__f = Fernet(config['VERIF_KEY'])
        self.__decrypt = self.__f.decrypt(self.__image.encode('utf-8')).decode('utf-8')

    def decrypt(self, var):
        return var == self.__decrypt
    
    def config(self):
        return {
            'hash': self.__image,
        }