import dotenv
import psycopg2
from psycopg2.extras import RealDictCursor
import secrets
from datetime import datetime
from controllers import crypter, tokens, payment

class PublicArtist:
    def __init__(self, username=None):
        self.__config = dotenv.dotenv_values('.env')
        self.__con = psycopg2.connect(self.__config['DB_URL'])
        self.__cur = self.__con.cursor(cursor_factory=RealDictCursor)
        self.info = None
        if username:
            self.__cur.execute('SELECT "U_id" FROM users WHERE "U_name"=%s', [username])
            result = self.__cur.fetchone()
            print('ID GOT FROM username : ', username,"==>", result)
            self.info = self.artist_infos([result['U_id']], infos=[
        'name', 'thumbURL', 'website', 'followers', 'followings', 'publis', 'id', 'email', 'e_visible'
    ] )

    def artist_infos(self, artistId: list, userId=None, infos=['id','name','thumbURL']):
        query = "SELECT "
        for x in infos:
            query+=f'\"U_{x}\",'
        query = query[:-1] + """ FROM users WHERE """
        for x in artistId:
            query+=" \"U_id\"=%s OR"
        query = query[:-2]
        self.__cur.execute(query, artistId)
        infos = self.__cur.fetchall()
        return infos

    def len_published(self, artistId):
        self.__cur.execute('SELECT COUNT(*) FROM songs WHERE "S_artist_id"=%s', [artistId])
        return self.__cur.fetchone()['count']

    def get_email(self, artistId):
        self.__cur.execute('SELECT "U_email" FROM users WHERE "U_id"=%s', artistId)
        return self.__cur.fetchone()['U_email']

class User:
    @staticmethod
    def new(infos):
        """REQUIRED IN --infos : name, email, password and nothing has to be crypted """
        config=dotenv.dotenv_values('.env')
        con = psycopg2.connect(config['DB_URL'])
        cur = con.cursor(cursor_factory=RealDictCursor)
        token = secrets.token_hex(16)

        infos['password'] = crypter.hash(infos['password'])
        querry = "INSERT INTO users( "
        for x in infos:
            querry+=f""" "U_{x}","""
        querry = querry[:-1] +') VALUES('
        values= []
        for y in infos:
            values.append(infos[y])
            querry+=f"%s,"
        querry = querry[:-1] +') RETURNING "U_id";'
        try:
            cur.execute(querry, values)
        except psycopg2.errors.UniqueViolation as e:
            return 'exists'
        except Exception as e:
            return False
        row = cur.fetchone()
        con.commit()
        con.close()
        token = User.add_token(token,row['U_id'] )

        return User(row['U_id'])
    
    @staticmethod
    def get_id(email):
        config=dotenv.dotenv_values('.env')
        con = psycopg2.connect(config['DB_URL'])
        cur = con.cursor(cursor_factory=RealDictCursor)

        cur.execute('SELECT "U_id" FROM users WHERE "U_email"=%s', [email])
        Uid = cur.fetchone()['U_id']
        con.close()
        return Uid

    @staticmethod
    def get_username(uid):
        config=dotenv.dotenv_values('.env')
        con = psycopg2.connect(config['DB_URL'])
        cur = con.cursor(cursor_factory=RealDictCursor)

        cur.execute('SELECT "U_name" FROM users WHERE "U_id"=%s', [uid])
        name = cur.fetchone()['U_name']
        con.close()
        return name
    
    @staticmethod
    def is_taken(username):
        config=dotenv.dotenv_values('.env')
        con = psycopg2.connect(config['DB_URL'])
        cur = con.cursor(cursor_factory=RealDictCursor)

        cur.execute('SELECT COUNT(*) FROM users WHERE "U_name"=%s', [username])
        count = cur.fetchone()['count']
        con.close()
        return count > 0

    @staticmethod
    def add_token( token, id):
        return tokens.Token.new({'token':token,'elem_id':id})

    @staticmethod
    def login( email, password):
        config=dotenv.dotenv_values('.env')
        con = psycopg2.connect(config['DB_URL'])
        cur = con.cursor(cursor_factory=RealDictCursor)

        cur.execute(""" SELECT "U_id", "U_password", "U_ready" FROM users WHERE "U_email"=%s """, (email,))
        row = cur.fetchone()
        if not row:
            return False, 'email'
        if crypter.hash(password) == row['U_password'] and row['U_ready']:
            return True, User(row['U_id'])
        return False, 'password'

    # MAIN PART

    def __init__(self, id=None, metadata={}) -> None:
        self.id = id
        if not id:
            return None
        self.db_config=dotenv.dotenv_values('.env')
        self.con = psycopg2.connect(self.db_config['DB_URL'])
        self.cur = self.con.cursor(cursor_factory=RealDictCursor)
        self.c = payment.Client()
        for x in metadata:
            setattr(self, str(x)[1], metadata[x])
        if id:
            self.fill_user_infos()
    
    def fill_user_infos(self):
        try:
            self.cur.execute(f"""SELECT * FROM users WHERE "U_id"={self.id}""")
            infos = self.cur.fetchone()
            for x in infos:
                setattr(self, str(x).split('U_', 1)[1], infos[x])
        except Exception as e:
            print(e)
        
    def cancel(self):
        if self.type:
            return False
        self.cur.execute("""DELETE FROM users WHERE "U_id"=%s""",(self.id))
        self.con.commit()
        return True

    def edit(self, username, email='', e_visible='', d_visible='', website='', public_email=''):
        print('VISIBLE EMAIL : ', public_email)
        if email == '':
            email = self.email

        if e_visible == '':
            e_visible = self.e_visible

        if d_visible == '':
            d_visible = self.public_down
        
        if public_email == '':
            public_email = self.visible_email

        if website == '':
            website = self.website
        print('VISIBLE EMAIL : ', public_email)
        self.cur.execute("""
            UPDATE users SET 
                "U_email"=%s,
                "U_name"=%s,
                "U_e_visible"=%s,
                "U_public_down"=%s,
                "U_website"=%s,
                "U_visible_email"=%s
            WHERE "U_id"=%s
        """, [email, username, e_visible, d_visible, website, public_email, self.id])
        self.con.commit()
        self.fill_user_infos()
        return True
        

    def is_following(self, artistId):
        self.cur.execute('SELECT COUNT(*) FROM followings WHERE "F_follower_id"=%s AND "F_artist_id"=%s', [self.id, artistId])
        return self.cur.fetchone()['count'] > 0

    def switch_follow(self, artistId):
        with open('controllers/sql_queries/switch_follow.sql','r') as query:
            liste_values = [int(artistId), self.id] * 5
            self.cur.execute(query.read(), liste_values)
            self.con.commit()
        self.fill_user_infos()
        return True

    def add_publis(self):
        self.cur.execute(
            """UPDATE users SET "U_publis"="U_publis" + 1 WHERE "U_id"=%s """
            , [self.id]
        )
        self.con.commit()
        return True

    def config(self):
        return {
            'id':self.id,
            'name':self.name,
            'email':self.email,
            'type':self.type,
            'cus_id':self.cus_id,
            'acc_id': self.acc_id,
            'website': self.website,
            'created_at': str(self.created_at),
            'followers': self.followers,
            'followings': self.followings,
            'thumbUrl':self.thumbURL,
            'premium_a': self.premium_a,
            'ready': self.ready,
            'public_down': self.public_down,
            'e_visible': self.e_visible,
            'visible_email': self.visible_email
        }

    def finalize_stripe(self, acc_tok, website, iban, email):
        acc = self.c.create_account(acc_tok, website, iban)
        self.cur.execute("""
            UPDATE users SET
            "U_acc_id"=%s,
            "U_ready"=true
            WHERE "U_email"=%s
        """, [acc.id, email])
        self.con.commit()
        return True


    def finalize(self, password, UType, thumbURL,visible,public_email, website='', Uid=None):
        if not Uid:
            Uid = self.id
        try:
            cus_id = self.c.create_cus(self.email, self.name)
            self.cur.execute(f"""
                UPDATE users SET
                    "U_website"=%s,
                    "U_password"=%s,
                    "U_cus_id"=%s,
                    "U_type"=%s,
                    "U_ready"=%s,
                    "U_thumbURL"=%s,
                    "U_e_visible"=%s,
                    "U_public_down"=%s,
                    "U_visible_email"=%s
                WHERE "U_id"=%s
            """, (
                website,
                crypter.hash(password),
                cus_id,
                UType,
                (UType=='C'),
                f'https://muse-fabric.s3.eu-west-3.amazonaws.com/thumbs/{thumbURL}',
                (visible['e']=='on'),
                (visible['d']=='on'),
                public_email,
                Uid
            ))
            self.con.commit()
            self.fill_user_infos()
            return True
        except Exception as e:
            return False

    def go_premium(self, userId):
        self.cur.execute('UPDATE users SET "U_premium_a"=true WHERE "U_id"=%s',[userId])
        self.con.commit()
        return True