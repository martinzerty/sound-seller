from datetime import datetime, timedelta
import dotenv
import psycopg2
from psycopg2.extras import RealDictCursor
import secrets
from controllers import crypter, users

class Token:

    @staticmethod
    def new(infos):
        """REQUIRED IN --infos : {token: str, end: date, elem_id: int}  and nothing has to be crypted """
        config=dotenv.dotenv_values('.env')
        con = psycopg2.connect(config['DB_URL'])
        cur = con.cursor(cursor_factory=RealDictCursor)
        token = secrets.token_hex(16)

        end = datetime.now() + timedelta(hours=int(config['DUREE_TOKEN']))
        infos['end']=end
        print('INFOS FROM TOKEN CLASS : ', infos)
        querry = "INSERT INTO tokens( "
        for x in infos:
            querry+=f""" "TOK_{x}","""
        querry = querry[:-1] +') VALUES('
        values= []
        for y in infos:
            values.append(infos[y])
            querry+=f"%s,"
        querry = querry[:-1] +') RETURNING "TOK_id";'
        cur.execute(querry, values)
        row = cur.fetchone()
        con.commit()
        con.close()
        return Token(row['TOK_id'])

    @staticmethod
    def retrieve_elem(token):
        config=dotenv.dotenv_values('.env')
        con = psycopg2.connect(config['DB_URL'])
        cur = con.cursor(cursor_factory=RealDictCursor)
        cur.execute('SELECT "TOK_elem_id" FROM tokens WHERE "TOK_token"=%s', [token])
        return cur.fetchone()['TOK_elem_id']

    @staticmethod
    def get_token(elem_id):
        config=dotenv.dotenv_values('.env')
        con = psycopg2.connect(config['DB_URL'])
        cur = con.cursor(cursor_factory=RealDictCursor)
        cur.execute(f"""SELECT "TOK_token", "TOK_id" FROM tokens WHERE "TOK_elem_id"={elem_id}""")
        row = cur.fetchone()
        con.close()
        return Token(row['TOK_id'])

    @staticmethod
    def verify_token(token):
        config=dotenv.dotenv_values('.env')
        con = psycopg2.connect(config['DB_URL'])
        cur = con.cursor(cursor_factory=RealDictCursor)
        cur.execute(f""" SELECT * FROM tokens WHERE "TOK_token"='{token}'; """)
        row = cur.fetchone()
        con.close()
        if not row:
            return False, 'TOK_late'
        if row["TOK_end"] < datetime.now():
            return False, 'TOK_late'
        else:
            return True, users.User(row["TOK_elem_id"])

    @staticmethod
    def reset(token):
        config=dotenv.dotenv_values('.env')
        con = psycopg2.connect(config['DB_URL'])
        cur = con.cursor(cursor_factory=RealDictCursor)
        cur.execute('DELETE FROM tokens WHERE "TOK_token"=%s', [token])
        con.commit()
        elemId = Token.retrieve_elem(token)
        return Token.new({'token': crypter.gen_hash(),'elem_id': elemId})


    def __init__(self, id, metadata={}) -> None:
        self.id = id
        self.db_config=dotenv.dotenv_values('.env')
        self.con = psycopg2.connect(self.db_config['DB_URL'])
        self.cur = self.con.cursor(cursor_factory=RealDictCursor)

        for x in metadata:
            setattr(self, str(x)[1], metadata[x])

        self.fill_infos()
    
    def fill_infos(self):
        try:
            self.cur.execute(f"""SELECT * FROM tokens WHERE "TOK_id"={self.id}""")
            infos = self.cur.fetchone()
            for x in infos:
                setattr(self, str(x).split('TOK_')[1], infos[x])
        except Exception as e:
            print(e)

    def config(self):
        return {
            'id':self.id,
            'token':self.token,
            'create':self.create,
            'end':self.end,
            'elem_id':self.elem_id
        }